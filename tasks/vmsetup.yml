---
- name: Add kubernetes YUM repository
  yum_repository:
    name: Kubernetes
    description: K8s YUM repo
    baseurl: https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
    gpgcheck: yes
    repo_gpgcheck: yes
    gpgkey:
      - https://packages.cloud.google.com/yum/doc/yum-key.gpg
      - https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

- name: Check kubernetes YUM requisites
  package:
    name: "{{ pkg_list | join(',') }}"
    state: present
  notify: restart kubelet
  vars:
    pkg_list:
      - kubectl
      - kubeadm
      - kubernetes-cni
      - kubelet
      - bash-completion
      - ipvsadm
      - conntrack-tools

- name: disable SELinux
  selinux: state=disabled

- name: Remove swapfile from /etc/fstab
  mount: name=swap fstype=swap state=absent

- name: Disable swap
  command: swapoff -a
  when: ansible_swaptotal_mb > 0

- name: stop and disable firewalld
  systemd: name=firewalld state=stopped enabled=no

- name: copy sysctl net conf
  copy:
    src: net-sysctl.conf
    dest: /etc/sysctl.d/66-k8s.conf

- name: set iptables in sysctl
  command: "sysctl -f /etc/sysctl.d/66-k8s.conf"

- name: copy modules net conf
  copy:
    src: k8s-modules.conf
    dest: /etc/modules-load.d/kubernetes.conf

- name: load IPVS modules
  modprobe:
    name: "{{ item }}"
    state: present
  loop:
    - br_netfilter
    - ip_vs
    - ip_vs_rr
    - ip_vs_wrr
    - ip_vs_sh
    - nf_conntrack
    - nf_conntrack_ipv4

- name: configure ipvs
  lineinfile:
    path: /etc/modprobe.d/ip_vs.conf
    line: options ip_vs conn_tab_bits=20
    create: yes

- name: flush iptables
  shell: >
    ipvsadm --clear ;
    iptables --flush && iptables -tnat --flush ;
    iptables -t mangle -F && iptables -X

- name: stop and disable postfix
  systemd: name=postfix state=stopped enabled=no
  ignore_errors: yes

- name: copy docker configuration file
  copy:
    src: daemon.json
    dest: /etc/docker/daemon.json
    owner: root
    group: root
    mode: u+rw,g-wx,o-rwx

- name: Ensure docker systemd configuration directory exists
  file:
    path: /etc/systemd/system/docker.service.d
    state: directory

- name: copy docker systemd configuration file
  copy:
    src: docker-systemd.conf
    dest: /etc/systemd/system/docker.service.d/override.conf
    owner: root
    group: root
    mode: u+rw,g-wx,o-rwx

- name: start and enable docker
  systemd: name=docker state=started enabled=yes daemon_reload=yes

- name: enable kubelet
  systemd: name=kubelet enabled=yes

# /etc/systemd/system/kubelet.service.d does not exist on centos7 NEW

- name: set kubelet configuration (systemd take1)
  copy:
    content: 'Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"'
    dest: /etc/systemd/system/kubelet.service.d/90-local-extras.conf
  ignore_errors: yes

- name: set kubelet configuration (systemd take2)
  copy:
    content: 'Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"'
    dest: /usr/lib/systemd/system/kubelet.service.d/90-local-extras.conf
  ignore_errors: yes

- name: systemd daemon-reload and restart kubelet
  systemd: name=kubelet state=restarted daemon_reload=yes
