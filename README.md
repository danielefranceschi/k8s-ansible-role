# K8S-DEV

Ansible role for fast installing a dev-grade non-HA kubernetes cluster: with Rancher console, NFS CSI, Ingress, a choice of CNI
- single master
- choice of network plugins
- helm3 on the master
- haproxy ingress controller
- rancher management with ingress (optional)
- nfs local provisioner (optional)

## Requirements

- some Centos7 VMs with `docker-ce` installed
- an inventory like `example-inventory`
- Ansible 2.7

# Variables

- `helm_version`: Helm version to install on the master (default `v3.3.4`)
- `installRancher`: installs Rancher management console (default: `true`)
- `rancher_console_virtual_host`: rancher dashboard virtual host (default: `mastername.domain`)
- `networkingPlugin`: CNI driver plugin (`weave`, `kuberouter` (default), `canal`, `calico`, `flannel`)
- `installDashboard`: installs kubernetes standard dashboard (default: `false`)
- `dashboardvirtualhost`: dashboard virtual host name (es. `kube-dashboard.example.com`)
- `installNFSProvisioner`: install NFS Volume Provisioner on the infra node (default: `false`)
- `NFS_local_directory`: local infra node path of big partition (default: `/mnt/exports`)

# Sample playbook

```yaml
---
- hosts: all
  become: yes
  roles:
     - k8s-dev
```

# More storage options

NFS provisioning can be done on:
- a PVC (set `install_NFS_over_FS=true`, `NFS_over_FS_volume_size=128Gi`, `NFS_over_FS_storageclass=xxx` )
- a pre-provisioned PV (set `install_NFS_over_volume=true` and `NFS_volume_name=big-volume`)
- a local directory on the infra node (set `install_NFS_over_local=true` and `NFS_local_directory=/mnt/exports`)

## TODO

- [ ] choose K8s version  
- [ ] use K3s
